# Lufi container

## Description
What is [lufi](https://framagit.org/luc/lufi) ?

Lufi means Let's Upload that FIle.

It stores files and allows you to download them.

Is that all? No. All the files are encrypted by the browser! It means that your files never leave your computer unencrypted. The administrator of the Lufi instance you use will not be able to see what is in your file, neither will your network administrator, or your ISP.

By default, this container uses SQLite for data storage but it can use an existing MySQL/MariaDB or PostgreSQL database.

**This image does not contain root processes. A lufi user (uid=1000) and lufi group (gid=1000) is created**

This image is based on lufi docker images:

- https://github.com/xataz/docker-lufi
- https://github.com/victor-rds/docker-lufi

## Configuration
### Environments
- **INSTANCE_NAME** : *name of the instance (default : Lufi)*
- **WEBROOT** : *webroot of lufi (default : /)*
- **SECRET** : *random string used to encrypt cookies (default : will be generated on the first run)*
- **MAX_FILE_SIZE** : *maximum file size of an uploaded file in bytes (default : 1024x1024x1024 (1 GiB))*
- **CONTACT** : *Lufi isntance contact (default : contact@domain.tld)*
- **REPORT** : *URL or an email address to receive file reports (default : report@domain.tld)*
- **DEFAULT_DELAY** : *default time limit for files in days (default : 1 (0 for unlimited))*
- **MAX_DELAY** : *number of days after which the files will be deleted (default : 0 for unlimited)*
- **THEME** : *theme for lufi (default : default)*
- **ALLOW_PWD_ON_FILES** : *enable download password (default : 1 (0 => disable, 1 => enable))*
- **DB_TYPE** : *type of database (default : sqlite (choices : sqlite, postgresql, mysql (or mariadb)))*
- **DB_NAME** : *name of the database (default : lufi)*
- **DB_HOST** : *hostname/ip of the database (default : 127.0.0.1)*
- **DB_USER** : *database username (default : lufi)*
- **DB_PASSWORD** : *database password (default : Tpr1qJEMqtMo8OhxnkmhNMOlf)*
- **DB_PORT** : *port for PostgreSQL or MySQL/MariaDB database (default : 5432 for PostgreSQL or 3306 for MySQL/MariaDB)*

### Volumes
- **/usr/lufi/lufi.conf** : *lufi's configuration file is here*
- **/usr/lufi/data** : *lufi's database is here*
- **/usr/lufi/files** : *location of uploaded files*

### Ports
- **8081**

## Usage
### Build this image
```shell
$ podman build -f Dockerfile -t lufi-alpine .
```

### Run with SQLite database
```shell
# Create a Lufi application container
$ podman run -d -p 8081:8081 \
                --name lufi-sqlite \
                --env INSTANCE_NAME="Drop with SQLite" \
                lufi-alpine

# Generate Systemd service file (optional)
$ podman generate systemd --new --files --name lufi-sqlite
```
URI access : http://localhost:8081

### Run with PostgreSQL database
```shell
# Create a pod
$ podman pod create --name lufi-postgresql -p 9080:8081

# Create a PostgreSQL database
$ podman run -dt --name lufi-postgresql-db \
                 --pod lufi-postgresql \
                 --restart on-failure \
                 --env POSTGRES_PASSWORD=MyP455w0rD \
                 --env POSTGRES_DB=db \
                 --env POSTGRES_USER=lufi_user \
                 postgres:14-alpine

# Create a Lufi application container
$ podman run -dt --name lufi-postgresql-app \
                 --pod lufi-postgresql \
                 --restart on-failure \
                 --env INSTANCE_NAME="Drop with PostgreSQL" \
                 --env MAX_FILE_SIZE=3*1024*1024*1024 \
                 --env DB_TYPE=postgresql \
                 --env DB_NAME=db \
                 --env DB_USER=lufi_user \
                 --env DB_PASSWORD=MyP455w0rD \
                 lufi-alpine

# Generate Systemd service file (optional)
$ podman generate systemd --new --files --name lufi-postgresql
```
URI access : http://localhost:9080



### Run with MariaDB database
```shell
# Create a pod
$ podman pod create --name lufi-mariadb -p 9081:8081

# Create a MariaDB database
$ podman run -dt --name lufi-mariadb-db \
                 --pod lufi-mariadb \
                 --restart on-failure \
                 --env MARIADB_RANDOM_ROOT_PASSWORD=yes \
                 --env MARIADB_PASSWORD=MyP455w0rD \
                 --env MARIADB_DATABASE=db \
                 --env MARIADB_USER=lufi_user \
                 mariadb:10

# Create a Lufi application container
$ podman run -dt --name lufi-mariadb-app \
                 --pod lufi-mariadb \
                 --restart on-failure \
                 --env INSTANCE_NAME="Drop with MariaDB" \
                 --env MAX_FILE_SIZE=3*1024*1024*1024 \
                 --env DB_TYPE=mariadb \
                 --env DB_NAME=db \
                 --env DB_USER=lufi_user \
                 --env DB_PASSWORD=MyP455w0rD \
                 lufi-alpine

# Generate Systemd service file (optional)
$ podman generate systemd --new --files --name lufi-mariadb
```
URI access : http://localhost:9081


## TODO
- Tag image
- Drag and Drop not work !
- Improve database section in startup file to use original **lufi.conf.template**

## Contributing
Any contributions are very welcome !
