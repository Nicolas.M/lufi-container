FROM alpine:3

ARG LUFI_VERSION=master

ENV LUFI_DIR=/usr/lufi \
    DB_TYPE=sqlite

LABEL description="lufi on alpine" \
      maintainer="Nicolas.M"

RUN apk add --update --no-cache --virtual .build-deps \
                build-base \
                libressl-dev \
                ca-certificates \
                tar \
                perl-dev \
                libidn-dev \
                perl-app-cpanminus \
                postgresql-dev \
                mariadb-dev \
                zlib-dev \
                wget && \
    apk add --update --no-cache \
                libressl \
                perl \
                libidn \
                perl-crypt-rijndael \
                perl-test-manifest \
                perl-net-ssleay \
                perl-lwp-protocol-https \
                perl-io-socket-ssl \
                postgresql-jit \
                mariadb-connector-c \
                git \
                zlib \
                tini \
                su-exec && \
    cpanm Carton && \
    git clone -b ${LUFI_VERSION} https://framagit.org/fiat-tux/hat-softwares/lufi.git --depth 1 ${LUFI_DIR} && \
    cd ${LUFI_DIR} && \
    echo "requires 'Mojo::mysql';" >> cpanfile && \
    mkdir -p /usr/lufi/local/lib/perl5/Crypt/ && \
    mkdir -p /usr/lufi/local/lib/perl5/auto/Crypt/Rijndael && \
    ln -s /usr/lib/perl5/vendor_perl/Crypt/Rijndael.pm /usr/lufi/local/lib/perl5/Crypt/Rijndael.pm && \
    ln -s /usr/lib/perl5/vendor_perl/auto/Crypt/Rijndael/Rijndael.so /usr/lufi/local/lib/perl5/auto/Crypt/Rijndael/Rijndael.so && \
    carton install --deployment --without=test --without=swift-storage --without=ldap && \
    #cp /usr/bin/mariadb-config /usr/bin/mariadb-config.backup && \
    apk del .build-deps && \
    rm -rf /var/cache/apk/* /root/.cpan* ${LUFI_DIR}/local/cache/* #&& \
    #mv /usr/bin/mariadb-config.backup /usr/bin/mariadb-config && \
    #ln -s /usr/bin/mariadb_config /usr/bin/mysql_config

WORKDIR ${LUFI_DIR}
VOLUME ${LUFI_DIR}/data ${LUFI_DIR}/files
EXPOSE 8081

COPY startup /usr/local/bin/startup
COPY lufi.conf.template ${LUFI_DIR}/lufi.conf.template
RUN chmod +x /usr/local/bin/startup

CMD ["/bin/sh", "/usr/local/bin/startup"]
